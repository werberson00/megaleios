## Requerimentos

1. Node && npm ou yarn
2. Angular/cli

---

## Como rodar o app

1. Navegue até a pasta app e execute npm install em seguida ng serve.
2. Abra a url http://localhost:4200.
