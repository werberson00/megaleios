import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { User } from '../auth/user';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MainService {

  readonly url: string = 'http://api.template.megaleios.com/api/v1/Profile'

  constructor(
    private http: HttpClient
  ) { }

  getUsers(): Observable<any[]> {
    return this.http.get<any[]>(`${this.url}/List?limit=30`)
      .pipe(
        catchError((e) => {
          console.log(e)
          return throwError(e)
        })
      )
  }
}
