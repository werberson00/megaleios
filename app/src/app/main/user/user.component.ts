import { Component, OnInit, ViewChild } from '@angular/core';
import { MainService } from '../main.service';
import { Observable } from 'rxjs';
import { User } from 'src/app/auth/user';
import { DataSource } from '@angular/cdk/table';

const ELEMENT_DATA: User[] = []

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  displayedColumns: string[] = ['name', 'login', 'email', 'cpf'];
  dataSource = [];
  user: any[]

  constructor(
    private mainService: MainService
  ) {}

  ngOnInit() {
    this.users()
  }

  users() {
    this.mainService.getUsers()
      .subscribe(
        (u: any) => {
          this.dataSource = u.data
        },
        (err) => {
          console.log(err)
        }
      )
  }

}



