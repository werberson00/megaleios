import { Component } from '@angular/core';
import { AuthService } from './auth/auth.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  authenticated$: Observable<boolean>

  constructor(
    private authService: AuthService,
    private router: Router
  ) {
    this.authenticated$ = this.authService.isAuthenticated()
  }

  logout() {
    this.authService.logout()
    this.router.navigateByUrl('/auth/login')
  }

}
