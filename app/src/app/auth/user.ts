export interface User {
  password: string
  fullName: string
  login: string
  email: string
  cpf: string
  phone: string
}
