import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup = this.fb.group({
    'login': ['', [Validators.required]],
    'password': ['', [Validators.required, Validators.minLength(6)]]
  })

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private snackBar: MatSnackBar,
    private authService: AuthService
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    const credentials = this.loginForm.value
    console.log(credentials)
    this.authService.login(credentials)
      .subscribe(
        user => {
          this.snackBar.open(
            'Usuário logado com sucesso',
            'OK',
            {duration: 2000}
          )
          this.router.navigateByUrl('/main/users')
        },
        error => {
          console.log(error)
          this.snackBar.open(
            error.error.message,
            'OK',
            {duration: 2000}
          )
        }
      )
  }

}
