import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { User } from './user';
import { tap } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  readonly url: string = 'http://api.template.megaleios.com/api/v1/Profile'

  private subjUser$: BehaviorSubject<User> = new BehaviorSubject(null)
  private subjLoggedIn$: BehaviorSubject<boolean> = new BehaviorSubject(false)

  constructor(
    private http: HttpClient
  ) { }

  register(user: User): Observable<User> {
    return this.http.post<User>(`${this.url}/Register`, user)
  }

  login(credentials: {email: string, password: string}): Observable<any> {
    return this.http.post<any>(`${this.url}/Token`, credentials)
      .pipe(
        tap((u: any) => {
          localStorage.setItem('token', u.data.access_token)
          this.subjLoggedIn$.next(true)
          this.subjUser$.next(u)
        })
      )
  }

  isAuthenticated(): Observable<boolean> {
    let token = localStorage.getItem('token')
    if (token) {
      return of(true)
    }
    return this.subjLoggedIn$.asObservable()
  }

  logout() {
    localStorage.removeItem('token')
    this.subjLoggedIn$.next(false)
    this.subjUser$.next(null)
  }
}
