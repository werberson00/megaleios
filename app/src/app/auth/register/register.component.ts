import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { User } from '../user';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  formRegister = this.fb.group({
      'fullname': ['', [Validators.required]],
      'email': ['', [Validators.required, Validators.email]],
      'login': ['', [Validators.required]],
      'cpf': ['', [Validators.required, Validators.minLength(11), Validators.maxLength(11)]],
      'phone': ['', [Validators.required, Validators.minLength(7)]],
      'password1': ['', [Validators.required, Validators.minLength(6)]],
      'password2': ['', [Validators.required, Validators.minLength(6)]]
  }, { validator: this.matchingPasswords })

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    console.log(this.formRegister.value)
    let u: User = {
      ...this.formRegister.value,
      password: this.formRegister.value.password1
    }
    this.authService.register(u)
      .subscribe(
        user => {
          console.log(user)
          this.snackBar.open(
            'Usuário cadastrado com sucesso',
            'OK',
            {duration: 2000}
          )
          this.router.navigateByUrl('/auth/login')
        },
        err => {
          console.log(err)
          this.snackBar.open(err.error.message,'OK',{duration: 2000}
          )
        }
      )
  }

  matchingPasswords(group: FormGroup) {
    if (group) {
      const password1 = group.controls['password1'].value
      const password2 = group.controls['password2'].value
      if (password1 == password2) {
        return null
      }
    }
    return {matching: false}
  }

}
